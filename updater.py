import requests
import json
import datetime
import argparse

from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, CP437_FONT, TINY_FONT, SINCLAIR_FONT, LCD_FONT


def response_to_departure(response):
    response_object = json.loads(response.text)
    return response_object["departureList"]["departure"]["dateTime"]


def departure_to_datetime(departure_time):
    return datetime.datetime(int(departure_time["year"]), int(departure_time["month"]), int(departure_time["day"]),
                             int(departure_time["hour"]), int(departure_time["minute"]), 0)


def minutes_from_now_until(point_in_future):
    return int((point_in_future - datetime.datetime.now()).total_seconds() / 60)


def time_in_range(end, x):
    """Return true if x is in the range [start, end]"""
    return x <= end


def print_minutes_until_departure(device, n, block_orientation, rotate, url, end_time):
    esterbach_dm = requests.get(url)
    minutes_to_go = minutes_from_now_until(departure_to_datetime(response_to_departure(esterbach_dm)))
    # create matrix device

    print("Created device")

    # start demo
    show_message(device, str(minutes_to_go), fill="white", font=proportional(CP437_FONT), scroll_delay=0.5)
    print(minutes_to_go)
    time_in_range(end_time, datetime.datetime.now().time()) \
        and print_minutes_until_departure(device, n, block_orientation, rotate, url, end_time)


def demo(n, block_orientation, rotate):
    stop = "60501180"
    line = "esg:02033:E:R:n17"
    url = "http://www.linzag.at/static/XML_DM_REQUEST?&locationServerActive=1&stateless=1&type_dm=any&name_dm=" \
          + stop + "&line=" + line + "&mode=direct&outputFormat=json&limit=1"

    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=n or 4, block_orientation=block_orientation or -90, rotate=rotate or 0)
    device.contrast(60)
    end_time = datetime.time(20, 59, 0)
    print_minutes_until_departure(device, n, block_orientation, rotate, url, end_time)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='matrix_demo arguments',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--cascaded', '-n', type=int, default=4, help='Number of cascaded MAX7219 LED matrices')
    parser.add_argument('--block-orientation', type=int, default=-90, choices=[0, 90, -90], help='Corrects block orientation when wired vertically')
    parser.add_argument('--rotate', type=int, default=0, choices=[0, 1, 2, 3], help='Rotate display 0=0 , 1=90 , 2=180 , 3=270 ')

    args = parser.parse_args()

    try:
        demo( args.cascaded, args.block_orientation, args.rotate)
    except KeyboardInterrupt:
        pass

